import java.util.LinkedList;

public class LongStack {

	private LinkedList<Long> magasin;

	
   public static void main (String[] argum) {

   }

   LongStack() {
     
	   magasin = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
	   LongStack tmp = new LongStack();
	   tmp.magasin = (LinkedList<Long>) magasin.clone();
	   return tmp;
	   // TODO!!! Your code here!
   }

   public boolean stEmpty() {
	   return (magasin.size() == 0);
   }

   public void push (long a) {
	   magasin.addFirst(a);
      
   }

   public long pop() {
	   if (stEmpty())
	         throw new IndexOutOfBoundsException ("magasini alataitumine");
	   return magasin.removeFirst();
	     
   } // pop

   public void op (String s) {
	   long op2 = pop();
	   long op1 = pop();
	   
	  if (s.equals ("+")) push (op1 + op2);
      if (s.equals ("-")) push (op1 - op2);
      if (s.equals ("*")) push (op1 * op2);
      if (s.equals ("/")) push (op1 / op2);
	   
      // TODO!!!
   }
  
   public long tos() {
	   if (stEmpty())
	         throw new IndexOutOfBoundsException ("magasini alataitumine");
	   return magasin.getFirst();
   }

   @Override
   public boolean equals (Object o) {
	   if (((LongStack)o).magasin.size() != magasin.size()) return false;
		  for (int i = 0; i < magasin.size(); i++) {
			  if (((LongStack)o).magasin.get(i) != magasin.get(i)) return false;
		  }
	    return true;
	   
   }

   @Override
   public String toString() {
	   if (stEmpty()) return "T�hi";
		  StringBuffer b = new StringBuffer();
	    for (int i=(magasin.size()-1); i>=0; i--)
	      b.append (String.valueOf (magasin.get(i)) + " ");
	      return b.toString();
   }

   public static long interpret (String pol) throws RuntimeException {
	   String operators = "+-*/";
     LongStack stack = new LongStack();
		 pol = pol.replace("\t", "");
		 pol = pol.trim();
		 String[] elements = pol.split("\\s+");
		for (int i = 0; i < elements.length; i++) {
			if (!operators.contains(elements[i])) {
				stack.push(Long.parseLong(elements[i].trim()));
			} else {
				long a = stack.pop();
				long b = stack.pop();
				switch (elements[i]) {
				case "+":
					stack.push(a + b);
					break;
				case "-":
					stack.push(b - a);
					break;
				case "*":
					stack.push(a * b);
					break;
				case "/":
					stack.push(b / a);
					break;
				}
			}
		}
		
		if (stack.magasin.size() > 1) throw new RuntimeException();
		return stack.pop();
   }

}
